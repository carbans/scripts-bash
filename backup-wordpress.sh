#!/bin/bash

#Author: Carlos Latorre Sánchez
#Email: clatorre@totesamburjassot.net
#Date: 18/02/2016
#Version: 0.2
#Description: Script para hacer un backup de los ficheros y la bd de una instalación de wordpress, si se añade al cron se ejecuta cada domingo
################### EJECUCION EN CRON ##########################################
### 00 5 * * 0 sh backup-wordpress.sh
################################################################################

SOURCE=$1
USER=$2
USERPASS=$3
DB=$4

DATE="$(date +%d-%m-%Y)"
ANO="$(date +%Y)"
MES="$(date +%m)"
DIA="$(date +%d)"

DEST="$(pwd)/backup/${ANO}/${MES}/${DIA}"

FILEBACKUP="$DEST/backup-file.tar.gz"
FILEDB="$DEST/backup-file.sql"


LOGPATH="$DEST/log"
LOGFILE="$LOGPATH/log.txt"


if [ $# -ne 4 ]
then
	
	echo -e "Numero de parametros incorrectos se requieren 4 parametros"
	echo -e "Sintaxis:"
	echo -e "$0 directorio userbd passbd namedb"
	echo -e "Ayuda:"
	echo -e "$0 -h"
	exit 1
fi

echo "El directorio que se va a copiar es: $SOURCE" >> $LOGFILE
echo "El usuario de la Base de Datos es: $USER" >> $LOGFILE
echo "La Base de Datos es: $DB" >> $LOGFILE
if [ -d $DEST ];
then
echo "[+] Si existe el directorio $DEST"
else
echo "[!] No existe el directorio $DEST"
echo "[!] Creando el directorio"
mkdir -p $DEST
fi

if [ -d $LOGPATH ];
then
echo "[+] Si existe el directorio $LOGPATH"
else
echo "[!] No existe el directorio $LOGPATH"
echo "[!] Creando el directorio"
mkdir -p $LOGPATH
fi

echo "[!] Creando el respaldo de los ficheros"
tar czfv $FILEBACKUP $SOURCE >> $LOGFILE

echo "[!] Creando un respaldo de la base de datos"
mysqldump -u $USER -p$USERPASS $DB > $FILEDB


echo "[+] El backup ha sido creado en el directorio $DEST"
echo "[+] El nombre del backup es $FILEBACKUP"
echo "[+] Hay un log del backup en $LOGPATH"

#Funciones
die () {
    echo >&2 "$@"
    exit 1
}
